/*
  ==============================================================================

    ScanMaster.h
    Created: 26 Aug 2016 9:14:35pm
    Author:  Nikolai

  ==============================================================================
*/

#ifndef SCANMASTER_H_INCLUDED
#define SCANMASTER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


class ScanMaster : public juce::Timer, public ChangeBroadcaster
{
public:
	ScanMaster(int pluginformat);
	~ScanMaster();
	void scanPath(File path);
	void scanFormat(int pluginFormat);
	virtual void timerCallback() override;

	double getProgress();
	String getPlugName();

private:
	void startScan();
	void scanComplete();
	void savePluginListToFile(bool saveFailed = true);
	KnownPluginList knownPluginList;
	AudioPluginFormatManager formatManager;
    std::unique_ptr<XmlElement> failedPluginList;

	//Scanner objects
	std::unique_ptr<PluginDirectoryScanner> dirScanner; //Scans for plugins, starts scan of next plugin on timer callback.
	bool dirScannerActive; //The dirscanner is active
	bool scanRunning;
	bool scanCompleted; //Just used to clear the progress bar
	String pluginScanned; //Name of the plugin beeing scanned
	StringArray failedPlugins;

	//ApplicationProperties* appProperties;
	File deadMansPedalFile;
	File currentScanPath;
    std::unique_ptr<PropertiesFile> pluginFile; //File on disk that saves the plugins found

};



#endif  // SCANMASTER_H_INCLUDED
