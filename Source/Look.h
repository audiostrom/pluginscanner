/*
  ==============================================================================

    Look.h
    Created: 22 Jan 2013 9:17:31pm
    Author:  Nikolai
    THIS IS THE AUDIOSTRØM LOOKANDFEEL CLASS
    IT IS USED IN ALL AUDIOSTROM JUCE-PRODUCTS
    IT IS MAINTAINED IN THE SANDBOX-PROJECT !!!
    SANDBOX-PROJECT SHOULD ALWAYS CONTAIN THE LATEST VERSION
  ==============================================================================
*/

#ifndef __VISUALLOOK_H_8B416538__
#define __VISUALLOOK_H_8B416538__

#include "../JuceLibraryCode/JuceHeader.h"


class AudiostromLookAndFeel : public LookAndFeel_V3
{
	public:
	
	static const Colour TitleBarColor;	
	static const Colour WhiteTextColor;
    static const Colour BlackTextColor;
	static const Colour GrayTextColor;
	
	static const Colour WindowBackground;
	static const Colour DialogBackground;
	static const Colour PanelBackground;
    static const Colour MainBackColor;
	static const Colour ProgressBarBack;
    
	static const Colour ASBlueColor;
	static const Colour ASPinkColor;
	static const Colour ASGreenColor;
    
	static const Colour InputBlue;
	static const Colour OutputOrange;
	static const Colour MidiColor;

	static const Colour OnRed;
	static const Colour OnGreen;
	static const Colour ActiveBlue;
	static const Colour ErrorRed;

	static const Colour ListBackColor;
	static const Colour ListSelectColor;

	static const Colour TreeViewItemBack;

	static const Colour TextBoxBackColor;
	static const Colour TextBoxSelectionColor;

	static const Colour MeterBack;

	AudiostromLookAndFeel();



	/** Draws the standard button. */
	virtual void drawButtonBackground (Graphics& g,
		Button& button,
		const Colour& backgroundColour,
		bool isMouseOverButton,
		bool isButtonDown) override;

	static void drawFlatButton(Graphics& g,
		Button& button,
		const Colour& backgroundColour,
		bool isMouseOverButton,
		bool isButtonDown) ;

	//Draw toggle button
	void drawToggleButton(Graphics&, ToggleButton& button, bool isMouseOverButton, bool isButtonDown) override;
	
	void drawTickBox(Graphics&, Component&,
		float x, float y, float w, float h,
		bool ticked, bool isEnabled, bool isMouseOverButton, bool isButtonDown) override;
	
	//Scroll bar
	void drawScrollbar(Graphics& g, ScrollBar& scrollbar, int x, int y, int width, int height,
		bool isScrollbarVertical, int thumbStartPosition, int thumbSize, bool isMouseOver, bool isMouseDown) override;
	
	//DocumentWindow
	//==============================================================================
	virtual void drawDocumentWindowTitleBar (DocumentWindow& window,
		Graphics& g, int w, int h,
		int titleSpaceX, int titleSpaceW,
		const Image* icon,
		bool drawTitleTextOnLeft) override;



	virtual Button* createDocumentWindowButton (int buttonType) override;


	virtual void positionDocumentWindowButtons (DocumentWindow& window,
		int titleBarX, int titleBarY,
		int titleBarW, int titleBarH,
		Button* minimiseButton,
		Button* maximiseButton,
		Button* closeButton,
		bool positionTitleBarButtonsOnLeft) override;

	//// Menu Bar
	virtual int getDefaultMenuBarHeight() override;

	virtual void drawMenuBarBackground (Graphics& g, int width, int height,
		bool isMouseOverBar,
		MenuBarComponent& menuBar) override;

	virtual Font getMenuBarFont (MenuBarComponent& menuBar, int itemIndex, const String& itemText) override;
    
	int getMenuBarItemWidth (MenuBarComponent& menuBar, int itemIndex, const String& itemText) override;

	virtual void drawMenuBarItem (Graphics& g,
		int width, int height,
		int itemIndex,
		const String& itemText,
		bool isMouseOverItem,
		bool isMenuOpen,
		bool isMouseOverBar,
		MenuBarComponent& menuBar) override;
    
    // ProgressBar
    /** Draws a progress bar.
     
     If the progress value is less than 0 or greater than 1.0, this should draw a spinning
     bar that fills the whole space (i.e. to say that the app is still busy but the progress
     isn't known). It can use the current time as a basis for playing an animation.
     
     (Used by progress bars in AlertWindow).
     */
    virtual void drawProgressBar (Graphics&, ProgressBar& progressBar,
                                  int width, int height,
                                  double progress, const String& textToShow) override;
	//TABLES
	//Table header
	virtual void drawTableHeaderBackground (Graphics&, TableHeaderComponent&) override;

	virtual void drawTableHeaderColumn(Graphics&, const String& columnName, int columnId,
		int width, int height,
		bool isMouseOver, bool isMouseDown,
		int columnFlags);
	//Paint table
	static void paintRowBackground(Graphics& g, int rowNumber, int width, int height, bool rowIsSelected);
	static void paintCell(Graphics& g, int rowNumber, int columnId, int width, int height, bool rowIsSelected);
	static Font getTableContentFont();
	static void setTableContentFont(Graphics& g);
    //AlertWindow
    void drawAlertBox (Graphics& g, AlertWindow& alert, const Rectangle<int>& textArea, TextLayout& textLayout) override;
    virtual Font getAlertWindowMessageFont() override;
    virtual Font getAlertWindowFont() override;
    //Utility
	static bool setTextFromInputBox(String DialogTitle, String TextTitle, String& text);
	//==============================================================================
	/** Fills the background of a popup menu component. */
	virtual void drawPopupMenuBackground(Graphics& g, int width, int height) override;
	virtual Font getPopupMenuFont() override;
    virtual void drawLevelMeter(Graphics& g, int width, int height, float level) override;

	//==============================================================================
	virtual void drawComboBox(Graphics& g, int width, int height,
		bool isButtonDown,
		int buttonX, int buttonY,
		int buttonW, int buttonH,
		ComboBox& box) override;

	virtual Font getComboBoxFont(ComboBox& box) override;

	//ListBox
    static void drawListboxItem(String Text, int rowNumber, Graphics& g, int width, int height, bool rowIsSelected, Colour overlayColor = Colours::transparentBlack, Colour fontColour = Colours::transparentBlack, int hOffset=3);
	Colour getLightColorFromHash(int hash);

	virtual void paintToolbarButtonBackground(Graphics&, int width, int height, bool isMouseOver, bool isMouseDown, ToolbarItemComponent&) override;

	virtual void paintToolbarBackground(Graphics&, int width, int height, Toolbar&) override;

    
    //SLIDER (Fader)
    void drawLinearSlider (Graphics&, int x, int y, int width, int height,
                           float sliderPos, float minSliderPos, float maxSliderPos,
                           const Slider::SliderStyle, Slider&) override;
    
private:
	Array<Colour> lightColorPalette;
};

// Special look and feel for styling controls that are used only in some cases
class SpecialLooks   : public AudiostromLookAndFeel
{
public:
	// TABS used for Options window
	virtual void drawTabButton (TabBarButton&, Graphics& g, bool isMouseOver, bool isMouseDown);
	virtual void drawTabButtonText (TabBarButton&, Graphics& g, bool isMouseOver, bool isMouseDown);
	virtual void createTabButtonShape (TabBarButton&, Path& path,  bool isMouseOver, bool isMouseDown);
	virtual void fillTabButtonShape (TabBarButton&, Graphics& g, const Path& path, bool isMouseOver, bool isMouseDown);
	virtual int getTabButtonBestWidth (TabBarButton&, int tabDepth);
	virtual int getTabButtonSpaceAroundImage();
	virtual int getTabButtonOverlap (int tabDepth);
private:
};

#endif  // __VISUALLOOK_H_8B416538__
