/*
  ==============================================================================

    IconButton.h
    Created: 1 Apr 2014 12:35:58pm
    Author:  Nikolai

  ==============================================================================
*/

#ifndef ICONBUTTON_H_INCLUDED
#define ICONBUTTON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class IconButton    : public Button
{
public:
	IconButton(const String buttonName, const String toolTip);
    ~IconButton();

	virtual void paintButton(Graphics& g, bool isMouseOverButton, bool isButtonDown) override;
    void resized() override;
	void setRound(bool value);
	void setPadding(float pad);
	void setImages(const  Drawable* normalImage, const Drawable* downImage = nullptr, const Drawable* onImage = nullptr);
	enum ColourIds
	{
		backgroundColourId = 0x1014011,  
		backgroundOnColourId = 0x1014012,  
	};
    void setOnColor(Colour cl);
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (IconButton)

	virtual void buttonStateChanged() override;

	virtual void enablementChanged() override;
    
    Drawable* currentImage;

    std::unique_ptr<Drawable> normalImage, downImage, onImage;
    
    float minCorenerSize;
	float padding;
	bool round;
	bool roundedCorners;
};


#endif  // ICONBUTTON_H_INCLUDED
