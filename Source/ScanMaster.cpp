/*
  ==============================================================================

    ScanMaster.cpp
    Created: 26 Aug 2016 9:14:35pm
    Author:  Nikolai

  ==============================================================================
*/

#include "ScanMaster.h"

ScanMaster::ScanMaster(int pluginformat) :
	dirScanner(nullptr),
	dirScannerActive(false),
    scanRunning(false),
	scanCompleted(false)
{
	
	
	
	deadMansPedalFile = File(File::getSpecialLocation(File::userApplicationDataDirectory));
#if JUCE_MAC
	deadMansPedalFile = deadMansPedalFile.getChildFile("Application Support/audiostrom/LiveProfessor 2/CrashedPlugs.xml");
#else
	deadMansPedalFile = deadMansPedalFile.getChildFile("audiostrom/LiveProfessor 2/CrashedPlugs.xml");
#endif	


	formatManager.addDefaultFormats();


	File pluginFileLocation(File::getSpecialLocation(File::userApplicationDataDirectory));
#if JUCE_MAC
	pluginFileLocation = pluginFileLocation.getChildFile("Application Support/audiostrom/LiveProfessor 2/Plugins.xml");
#else 
#if JUCE_64BIT
	pluginFileLocation = pluginFileLocation.getChildFile("audiostrom/LiveProfessor 2/PluginsX64.xml");
#else
	pluginFileLocation = pluginFileLocation.getChildFile("audiostrom/LiveProfessor 2/Plugins.xml");
#endif	
#endif	

	PropertiesFile::Options O;
	O.osxLibrarySubFolder = "Application Support";
	O.storageFormat = PropertiesFile::StorageFormat::storeAsXML;
	O.commonToAllUsers = false;

	pluginFile.reset(new PropertiesFile(pluginFileLocation, O));

	std::unique_ptr<XmlElement> savedPluginList(pluginFile->getXmlValue("KnownPluginList"));

	if (savedPluginList != nullptr)
		knownPluginList.recreateFromXml(*savedPluginList);

	if (pluginformat >-1)
	{
		scanFormat(pluginformat);
	}
}

ScanMaster::~ScanMaster()
{
	stopTimer();
}

/*

Greia no: Må kunne velge path for søk

*/
void ScanMaster::scanFormat(int pluginFormat)
{
	AudioPluginFormat* format = formatManager.getFormat(pluginFormat);
	if (format != nullptr)
	{
		

		FileSearchPath paths = format->getDefaultLocationsToSearch();
		if (paths.getNumPaths() > 0)
		{
			//This format needs paths, show dialo
			paths = pluginFile->getValue("lastPluginScanPath_" + format->getName(), paths.toString());
			
			std::unique_ptr<FileSearchPathListComponent> pathList;
			pathList.reset(new FileSearchPathListComponent());
			pathList->setSize(500, 300);
			pathList->setPath(paths);

            std::unique_ptr<AlertWindow> window(new AlertWindow(TRANS("Select folders to scan..."), String(), AlertWindow::NoIcon));
            
			window->addCustomComponent(pathList.get());
			window->addButton(TRANS("Scan"), 1, KeyPress(KeyPress::returnKey));
			window->addButton(TRANS("Cancel"), 0, KeyPress(KeyPress::escapeKey));
			window->toFront(true);
            window->setAlwaysOnTop(true);
            window->runModalLoop();
	
			paths = pathList->getPath();
			//Save paths to file:
			pluginFile->setValue("lastPluginScanPath_" + format->getName(), paths.toString());
		}
		
		
		

		if (format != nullptr)
		{
		
			dirScanner.reset(new PluginDirectoryScanner(knownPluginList, *format, paths, true, deadMansPedalFile));

			startScan();
            
		}
	}

}

//Never used:
void ScanMaster::scanPath(juce::File path)
{
	//Todo, try abort search here
	int formatIndex = 0;
	AudioPluginFormat* format = formatManager.getFormat(formatIndex);
	String s = path.getFullPathName();
	DBG("Scan path " + s);
	FileSearchPath paths;
	paths.addIfNotAlreadyThere(path);

	if (format != nullptr)
	{
		currentScanPath = path;
		dirScanner.reset(new PluginDirectoryScanner(knownPluginList, *format, paths, true, deadMansPedalFile));

		startScan();
	}
}

void ScanMaster::timerCallback()
{
	if (!dirScannerActive)
	{
		dirScannerActive = true;
		sendChangeMessage();
		MessageManager::getInstance()->runDispatchLoopUntil(50);
		bool moreToDo = dirScanner->scanNextFile(true, pluginScanned);
		savePluginListToFile(false);
		if (!moreToDo)
		{
			stopTimer();
			failedPlugins = dirScanner->getFailedFiles();
			dirScanner = nullptr;
			scanComplete();
		}
		dirScannerActive = false;
	}
}

void ScanMaster::startScan()
{
	if (scanRunning) return;
	if (dirScanner != nullptr)
	{
		dirScannerActive = false;
		scanRunning = true;
		scanCompleted = false;
		failedPlugins.clear();
		startTimer(1);
	}
}

void ScanMaster::scanComplete()
{
	scanRunning = false;
	scanCompleted = true;
	sendChangeMessage();
	DBG("Scan complete");
	savePluginListToFile();
	JUCEApplication::quit();
}

void ScanMaster::savePluginListToFile(bool saveFailed)
{
    std::unique_ptr<XmlElement> savedPluginList(knownPluginList.createXml());

	if (savedPluginList != nullptr)
	{

	    pluginFile->setValue("KnownPluginList", savedPluginList.get());
	
	}
	if (saveFailed)
	{
		//Add the failed plugins to the list
		if (failedPlugins.size() > 0)
		{
            std::unique_ptr<XmlElement> failedPluginsXML;
			failedPluginsXML.reset(new XmlElement("FAILEDPLUGINS"));

			//add each failed plugins as child element in the XML tree
			for (int i = 0; i < failedPlugins.size(); i++)
			{
				String path = failedPlugins[i];
				if (path.isNotEmpty())
				{
					File badPluginFile(path);

					XmlElement* plugin = new XmlElement("PLUGIN");
					plugin->setAttribute("name", badPluginFile.getFileName());
					plugin->setAttribute("file", badPluginFile.getFullPathName());

					failedPluginsXML->addChildElement(plugin);
				}
			}

			//copy the whole xml tree to preferences
			pluginFile->setValue("FailedPluginList", failedPluginsXML.get());
		}
	}


	
	//Save file
	pluginFile->saveIfNeeded();
}

double ScanMaster::getProgress()
{
	if (scanCompleted)
	{
		return 1.0f;
	}
	
	if (scanRunning)
	{
		if (dirScanner != nullptr)
		{
			return dirScanner->getProgress();
		}
	}
	return 0.0f;
}

String ScanMaster::getPlugName()
{
	if (scanCompleted)
	{
		
		return "Scan Complete";
	}
	if (scanRunning)
	{
		if (dirScanner != nullptr)
		{
			return dirScanner->getNextPluginFileThatWillBeScanned();
			
		}
	}
	return "-";
}


