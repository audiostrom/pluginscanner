/*
  ==============================================================================

    VisualLook.cpp
    Created: 22 Jan 2013 9:17:31pm
    Author:  Nikolai

  ==============================================================================
*/

#include "Look.h"
#include "IconButton.h"


//Some handy global colours

const Colour AudiostromLookAndFeel::TitleBarColor(0xff464646);
const Colour AudiostromLookAndFeel::WhiteTextColor(0xffffffff);
const Colour AudiostromLookAndFeel::GrayTextColor(0xffB2B2B2);
const Colour AudiostromLookAndFeel::BlackTextColor(0xff000000);
const Colour AudiostromLookAndFeel::WindowBackground(0xff151515);
const Colour AudiostromLookAndFeel::DialogBackground(0xff3B3B3B);
const Colour AudiostromLookAndFeel::PanelBackground(0xff3B3B3B);
const Colour AudiostromLookAndFeel::ProgressBarBack(0xff202020);
const Colour AudiostromLookAndFeel::MainBackColor(Colour::fromRGB(28, 28, 32));

const Colour AudiostromLookAndFeel::ASBlueColor(0xff29AAE1);
const Colour AudiostromLookAndFeel::ASPinkColor(0xfff300ff);
const Colour AudiostromLookAndFeel::ASGreenColor(0xff99EB3F);

const Colour AudiostromLookAndFeel::InputBlue(0xff25a7fc);
const Colour AudiostromLookAndFeel::OutputOrange(0xffFCA525);
const Colour AudiostromLookAndFeel::MidiColor(Colour::fromRGB(245, 63, 214));

const Colour AudiostromLookAndFeel::OnRed(0xffFF097A);
const Colour AudiostromLookAndFeel::OnGreen(0xff99EB3F);
const Colour AudiostromLookAndFeel::ActiveBlue(Colour::fromRGB(55, 155, 192));
const Colour AudiostromLookAndFeel::ErrorRed(0xffFF097A);

const Colour AudiostromLookAndFeel::ListBackColor(Colour::fromRGB(33, 33, 37));
const Colour AudiostromLookAndFeel::ListSelectColor(0xffA9B3C2);

const Colour AudiostromLookAndFeel::TreeViewItemBack(Colour::fromRGB(40, 40, 40));

const Colour AudiostromLookAndFeel::TextBoxBackColor(0xff565759);
const Colour AudiostromLookAndFeel::TextBoxSelectionColor(0xffA9B3C2);

const Colour AudiostromLookAndFeel::MeterBack(0xff1f1f1f);


AudiostromLookAndFeel::AudiostromLookAndFeel()
{
//Set font

    setDefaultSansSerifTypefaceName("Arial");

//Set colours !!


//Button
    setColour(TextButton::ColourIds::buttonColourId, Colour(60, 60, 60));
    setColour(TextButton::ColourIds::textColourOffId, WhiteTextColor);
    setColour(TextButton::ColourIds::buttonOnColourId, OnGreen);
    setColour(TextButton::ColourIds::textColourOnId, Colour(0, 0, 0));

//ToggleButton
    setColour(ToggleButton::textColourId, WhiteTextColor);

//TextEditor
    setColour(TextEditor::ColourIds::backgroundColourId, TextBoxBackColor);
    setColour(TextEditor::ColourIds::highlightColourId, TextBoxSelectionColor);
    setColour(TextEditor::ColourIds::textColourId, WhiteTextColor);
    setColour(TextEditor::ColourIds::highlightedTextColourId, Colours::black);
    setColour(TextEditor::ColourIds::outlineColourId, Colours::black);
    setColour(TextEditor::ColourIds::focusedOutlineColourId, Colour(30, 30, 30));

//Combobox
    setColour(ComboBox::ColourIds::backgroundColourId, ListBackColor);
    setColour(ComboBox::ColourIds::outlineColourId, Colours::black);
    setColour(ComboBox::ColourIds::textColourId, WhiteTextColor);
    setColour(ComboBox::ColourIds::buttonColourId, Colour(60, 60, 60));
    setColour(ComboBox::ColourIds::buttonColourId, GrayTextColor);

//Scroll bars
    setColour(ScrollBar::ColourIds::thumbColourId, Colour(235, 235, 235));
    setColour(ScrollBar::ColourIds::trackColourId, Colour(60, 60, 60));
    setColour(ScrollBar::ColourIds::backgroundColourId, Colour(30, 30, 30));

//Table
    setColour(TableListBox::ColourIds::backgroundColourId, Colour(62, 62, 62));
    setColour(TableListBox::ColourIds::outlineColourId, Colour(0, 0, 0));
    setColour(TableListBox::ColourIds::textColourId, WhiteTextColor);


//Spin edit
    setColour(Slider::ColourIds::textBoxBackgroundColourId, TextBoxBackColor);
    setColour(Slider::ColourIds::textBoxHighlightColourId, TextBoxSelectionColor);
    setColour(Slider::ColourIds::textBoxOutlineColourId, Colours::black);
    setColour(Slider::ColourIds::textBoxTextColourId, WhiteTextColor);

//PopUp menu
    setColour(PopupMenu::ColourIds::backgroundColourId, Colour(240, 240, 240));
    setColour(PopupMenu::ColourIds::textColourId, (Colours::black));
    setColour(PopupMenu::ColourIds::highlightedBackgroundColourId,
              Colour(51, 153, 255)); //A Blue color, like on windows
    setColour(PopupMenu::ColourIds::highlightedTextColourId, Colours::white);

//TabbedButtonBar (in options etc)
    setColour(TabbedButtonBar::frontOutlineColourId, Colours::black);
//Listboxes
    setColour(ListBox::backgroundColourId, ListBackColor);
    setColour(ListBox::textColourId, WhiteTextColor);

//TreeView
    setColour(TreeView::backgroundColourId, ListBackColor);
    setColour(TreeView::ColourIds::selectedItemBackgroundColourId, ListSelectColor);

//Search paths list
    setColour(FileSearchPathListComponent::backgroundColourId, ListBackColor);

//AlertWindow
    setColour(AlertWindow::outlineColourId, Colours::black);
    setColour(AlertWindow::backgroundColourId, Colour(220, 220, 220));


//IconButton
    setColour(IconButton::backgroundColourId, Colour(111, 121, 138));
    setColour(IconButton::backgroundOnColourId, Colour(83, 83, 83));


//Label
    setColour(Label::ColourIds::textColourId, WhiteTextColor);
    setColour(Label::ColourIds::backgroundWhenEditingColourId, Colours::darkgrey);
    setColour(Label::ColourIds::textWhenEditingColourId, WhiteTextColor);
    setColour(Label::ColourIds::outlineWhenEditingColourId, ASBlueColor);


    setColour(Toolbar::backgroundColourId, DialogBackground);
    setColour(Toolbar::buttonMouseOverBackgroundColourId, DialogBackground.darker());
    setColour(Toolbar::buttonMouseDownBackgroundColourId, DialogBackground.brighter());


    lightColorPalette.add(Colour(79, 196, 246)); //Blue
    lightColorPalette.add(Colour(252, 177, 80)); //Organe
    lightColorPalette.add(Colour(227, 97, 118)); //Pink
    lightColorPalette.add(Colour(17, 168, 171)); //Greenish
    lightColorPalette.add(Colour(141, 230, 144));//LightGreen
    lightColorPalette.add(Colour(114, 140, 184));//LavendelBlue
    lightColorPalette.add(Colour(255, 214, 115));
    lightColorPalette.add(Colour(34, 91, 102));
    lightColorPalette.add(Colour(141, 191, 103));
}


//Buttons
//==============================================================================
void AudiostromLookAndFeel::drawButtonBackground(Graphics &g,
                                                 Button &button,
                                                 const Colour &backgroundColour,
                                                 bool isMouseOverButton,
                                                 bool isButtonDown)
{
    const int width = button.getWidth();
    const int height = button.getHeight();

    const float indent = 1;
    const int cornerSize = 1;

    //Draw the shape
    Path p;
    p.addRoundedRectangle(indent, indent, width - indent * 2.0f, height - indent * 2.0f, (float) cornerSize);
    //Get base color
    Colour bc(backgroundColour);

    //Hoover and down colors
    if (isMouseOverButton)
    {
        if (isButtonDown)
            bc = bc.brighter(0.2f);
        else
            bc = bc.brighter(0.03f);
    }

    //Fill background
    ColourGradient BackGrad(bc.brighter(0.07f), 0.0f, 0.0f, bc, 0.0f, (float) height, false);
    g.setGradientFill(BackGrad);
    g.fillPath(p);

    //Draw black stroke
    g.setColour(Colour::fromRGB(10, 10, 10));
    g.strokePath(p, PathStrokeType(1.0f, juce::PathStrokeType::mitered));

    //Bevel
    g.setColour(bc.brighter(0.3f));
    g.drawHorizontalLine(1, 2.0f, width - 2.0f);

    g.setColour(bc.brighter(0.1f));
    g.drawVerticalLine(1, 1.0f, (height - 2.0f));

    g.setColour(bc.darker(0.12f));
    g.drawHorizontalLine(height - 2, 2.0f, (float) width - 2.0f);

    g.drawVerticalLine(width - 2, 1.0f, height - 2.0f);


}


void AudiostromLookAndFeel::drawFlatButton(Graphics &g, Button &button, const Colour &backgroundColour,
                                           bool isMouseOverButton, bool isButtonDown)
{
    const int width = button.getWidth();
    const int height = button.getHeight();
    Colour bc(backgroundColour);
    if (isMouseOverButton)
    {
        if (isButtonDown)
            bc = bc.brighter(0.2f);
        else
            bc = bc.brighter(0.03f);
    }
    g.setColour(bc);
    g.fillRect(0, 0, width, height);
    g.setColour(Colours::black);
    g.drawRect(0, 0, width, height, 1);
}


void AudiostromLookAndFeel::drawToggleButton(Graphics &g, ToggleButton &button,
                                             bool isMouseOverButton, bool isButtonDown)
{

    float fontSize = jmin(15.0f, button.getHeight() * 0.75f);
    const float tickWidth = fontSize * 1.1f;

    drawTickBox(g, button, 4.0f, (button.getHeight() - tickWidth) * 0.5f,
                tickWidth, tickWidth,
                button.getToggleState(),
                button.isEnabled(),
                isMouseOverButton,
                isButtonDown);

    g.setColour(button.findColour(ToggleButton::textColourId));
    g.setFont(fontSize);

    if (!button.isEnabled())
        g.setOpacity(0.5f);

    const int textX = (int) tickWidth + 5;

    g.drawFittedText(button.getButtonText(),
                     textX, 0,
                     button.getWidth() - textX - 2, button.getHeight(),
                     Justification::centredLeft, 10);


}

void AudiostromLookAndFeel::drawTickBox(Graphics &g, Component & /*component*/,
                                        float x, float y, float w, float h,
                                        const bool ticked,
                                        const bool isEnabled,
                                        const bool isMouseOverButton,
                                        const bool isButtonDown)
{
    //const float boxSize = w * 0.7f;

    Colour tick = ticked ? ASGreenColor : Colour::fromRGB(100, 100, 100);
    if (!isEnabled) tick = tick.darker(0.5f);
    if (isButtonDown || isMouseOverButton) tick = tick.brighter();
    g.setColour(tick);
    g.fillRect(x + 1, y + 2, w - 3, h - 3);
    g.setColour(Colours::black);
    g.drawRect(x + 1, y + 2, w - 3, h - 3);


}


//Scroll bar

void AudiostromLookAndFeel::drawScrollbar(Graphics &g, ScrollBar &scrollbar, int x, int y, int width, int height,
                                          bool isScrollbarVertical, int thumbStartPosition, int thumbSize,
                                          bool isMouseOver, bool isMouseDown)
{
    Path thumbPath;
    g.setColour(scrollbar.findColour(ScrollBar::backgroundColourId, true));
    g.fillAll();
    if (thumbSize > 0)
    {
        const float thumbIndent = (isScrollbarVertical ? width : height) * 0.25f;
        const float thumbIndentx2 = thumbIndent * 2.0f;

        if (isScrollbarVertical)
            thumbPath.addRoundedRectangle(x + thumbIndent, thumbStartPosition + thumbIndent,
                                          width - thumbIndentx2, thumbSize - thumbIndentx2,
                                          (width - thumbIndentx2) * 0.5f);
        else
            thumbPath.addRoundedRectangle(thumbStartPosition + thumbIndent, y + thumbIndent,
                                          thumbSize - thumbIndentx2, height - thumbIndentx2,
                                          (height - thumbIndentx2) * 0.5f);
    }

    Colour thumbCol(scrollbar.findColour(ScrollBar::thumbColourId, true));

    if (isMouseOver || isMouseDown)
        thumbCol = ASBlueColor;//thumbCol.withMultipliedAlpha(2.0f);

    g.setColour(thumbCol);
    g.fillPath(thumbPath);

    g.setColour(thumbCol.contrasting((isMouseOver || isMouseDown) ? 0.2f : 0.1f));
    g.strokePath(thumbPath, PathStrokeType(1.0f));
}


//Document window
void AudiostromLookAndFeel::drawDocumentWindowTitleBar(DocumentWindow &window,
                                                       Graphics &g, int w, int h,
                                                       int titleSpaceX, int titleSpaceW,
                                                       const Image *icon,
                                                       bool drawTitleTextOnLeft)
{
//Branch if this is the main window as it is drawn differently:


    const bool isActive = window.isActiveWindow();

    /*g.setGradientFill (ColourGradient (window.getBackgroundColour(),
        0.0f, 0.0f,
        window.getBackgroundColour().contrasting (isActive ? 0.15f : 0.05f),
        0.0f, (float) h, false));*/

    isActive ? g.fillAll(TitleBarColor) : g.fillAll(TitleBarColor.brighter(0.1f));
    g.setColour(TitleBarColor.darker());
    g.drawLine(0.0f, (float) h, (float) w, (float) h, 1.0f);


    Font font(h * 0.55f);
    g.setFont(font);

    int textW = font.getStringWidth(window.getName());
    int iconW = 0;
    int iconH = 0;

    if (icon != nullptr)
    {
        iconH = (int) font.getHeight();
        iconW = icon->getWidth() * iconH / icon->getHeight() + 4;
    }

    textW = jmin(titleSpaceW, textW + iconW);
    int textX = drawTitleTextOnLeft ? titleSpaceX
                                    : jmax(titleSpaceX, (w - textW) / 2);

    if (textX + textW > titleSpaceX + titleSpaceW)
        textX = titleSpaceX + titleSpaceW - textW;

    if (icon != nullptr)
    {
        g.setOpacity(isActive ? 1.0f : 0.6f);
        g.drawImageWithin(*icon, textX, (h - iconH) / 2, iconW, iconH,
                          RectanglePlacement::centred, false);
        textX += iconW;
        textW -= iconW;
    }


    g.setColour(TitleBarColor.contrasting().darker());

    g.drawText(window.getName(), textX, 0, textW, h, Justification::centredLeft, true);
}





//////////////////////////////////////////////////////////////////////////

Button *AudiostromLookAndFeel::createDocumentWindowButton(int buttonType)
{
    Path shape;
    //const float crossThickness = 0.12f;

    if (buttonType == DocumentWindow::closeButton)
    {
        ImageButton *clButton = new ImageButton("closeWindowButton");
        clButton->setImages(true, false, true, ImageCache::getFromMemory(BinaryData::WindowBtnClose_png,
                                                                         BinaryData::WindowBtnClose_pngSize), 1.0000f,
                            Colour(0x0),
                            Image(), 1.0000f, Colour(0x0),
                            Image(), 1.0000f, Colour(0x0));
        return clButton;

    }
    else if (buttonType == DocumentWindow::minimiseButton)
    {
        ImageButton *miniButton = new ImageButton("closeWindowButton");
        miniButton->setImages(true, false, true, ImageCache::getFromMemory(BinaryData::WindowBtnMinimize_png,
                                                                           BinaryData::WindowBtnMinimize_pngSize),
                              1.0000f, Colour(0x0),
                              Image(), 1.0000f, Colour(0x0),
                              Image(), 1.0000f, Colour(0x0));
        return miniButton;
    }
    else if (buttonType == DocumentWindow::maximiseButton)
    {
        ImageButton *maxButton = new ImageButton("closeWindowButton");
        maxButton->setImages(true, false, true,
                             ImageCache::getFromMemory(BinaryData::WindowBtnMaximize_png,
                                                       BinaryData::WindowBtnMaximize_pngSize), 1.0000f, Colour(0x0),
                             Image(), 1.0000f, Colour(0x0),
                             ImageCache::getFromMemory(BinaryData::WindowBtnFullScreen_png,
                                                       BinaryData::WindowBtnFullScreen_pngSize), 1.0000f, Colour(0x0));
        return maxButton;
    }

    jassertfalse;
    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
void AudiostromLookAndFeel::positionDocumentWindowButtons(DocumentWindow &,
                                                          int titleBarX,
                                                          int titleBarY,
                                                          int titleBarW,
                                                          int titleBarH,
                                                          Button *minimiseButton,
                                                          Button *maximiseButton,
                                                          Button *closeButton,
                                                          bool positionTitleBarButtonsOnLeft)
{
    const int buttonW = titleBarH - titleBarH / 8;

    int x = positionTitleBarButtonsOnLeft ? titleBarX + 4
                                          : titleBarX + titleBarW - buttonW - buttonW / 4;

    if (closeButton != nullptr)
    {
        closeButton->setBounds(x, titleBarY, buttonW, titleBarH);
        x += positionTitleBarButtonsOnLeft ? buttonW : -(buttonW + buttonW / 4);
    }

    if (positionTitleBarButtonsOnLeft)
        std::swap(minimiseButton, maximiseButton);

    if (maximiseButton != nullptr)
    {
        maximiseButton->setBounds(x, titleBarY, buttonW, titleBarH);
        x += positionTitleBarButtonsOnLeft ? buttonW : -buttonW;
    }

    if (minimiseButton != nullptr)
        minimiseButton->setBounds(x, titleBarY, buttonW, titleBarH);
}

int AudiostromLookAndFeel::getDefaultMenuBarHeight()
{
    return 25;
}

//end Document window




//////////////////////////////////////////////////////////////////////////
// MenuBar

void AudiostromLookAndFeel::drawMenuBarBackground(Graphics &g, int width, int height,
                                                  bool, MenuBarComponent &menuBar)
{

    //g.fillAll (TitleBarColor);

}

Font AudiostromLookAndFeel::getMenuBarFont(MenuBarComponent &menuBar, int /*itemIndex*/, const String & /*itemText*/)
{
    return Font(menuBar.getHeight() * 0.8f);
}

int AudiostromLookAndFeel::getMenuBarItemWidth(MenuBarComponent &menuBar, int itemIndex, const String &itemText)
{
    return getMenuBarFont(menuBar, itemIndex, itemText)
                   .getStringWidth(itemText) + 12;
}

void AudiostromLookAndFeel::drawMenuBarItem(Graphics &g,
                                            int width, int height,
                                            int itemIndex,
                                            const String &itemText,
                                            bool isMouseOverItem,
                                            bool isMenuOpen,
                                            bool /*isMouseOverBar*/,
                                            MenuBarComponent &menuBar)
{
    g.setColour(WhiteTextColor);


    if (isMenuOpen || isMouseOverItem)
    {
        g.fillAll(TitleBarColor.brighter(0.1f));

    }


    g.setFont(getMenuBarFont(menuBar, itemIndex, itemText));
    g.drawFittedText(itemText, 0, 0, width, height, Justification::centred, 1);


}

////////// ProgressBar

//==============================================================================
void AudiostromLookAndFeel::drawProgressBar(Graphics &g, ProgressBar & /*progressBar*/,
                                            int width, int height,
                                            double progress, const String &textToShow)
{
    const Colour background = ProgressBarBack;//(progressBar.findColour (ProgressBar::backgroundColourId));
    const Colour foreground = ASGreenColor;//(progressBar.findColour (ProgressBar::foregroundColourId));

    g.fillAll(background);

    if (progress >= 0.0f && progress < 1.0f)
    {
        /* drawGlassLozenge (g, 1.0f, 1.0f,
                           (float) jlimit (0.0, width - 2.0, progress * (width - 2.0)),
                           (float) (height - 2),
                           foreground,
                           0.5f, 0.0f,
                           true, true, true, true);*/
        //g.drawRect(2,2, width-2, height-2);
        g.setColour(foreground);
        g.fillRect(2, 2, roundToInt((width - 4) * progress), height - 4);
    }
    else
    {
        // spinning bar..
        g.setColour(foreground.darker());
        g.fillRect(2, 2, width - 4, height - 4);
        const int stripeWidth = height * 2;
        const int position = (int) (Time::getMillisecondCounter() / 40) % stripeWidth;

        Path p;

        for (float x = (float) (-position); x < width + stripeWidth; x += stripeWidth)
            p.addQuadrilateral(x, 0.0f,
                               x + stripeWidth * 0.5f, 0.0f,
                               x, (float) height,
                               x - stripeWidth * 0.5f, (float) height);

        Image im(Image::ARGB, width, height, true);

        {
            Graphics g2(im);
            /*drawGlassLozenge (g2, 1.0f, 1.0f,
                              (float) (width - 2),
                              (float) (height - 2),
                              foreground,
                              0.5f, 0.0f,
                              true, true, true, true);*/
            g2.setColour(foreground);
            g2.fillRect(2, 2, width - 4, height - 4);
        }

        g.setTiledImageFill(im, 0, 0, 0.85f);
        g.fillPath(p);
    }

    if (textToShow.isNotEmpty())
    {
        g.setColour(Colour::contrasting(background, foreground));
        g.setFont(height * 0.6f);

        g.drawText(textToShow, 0, 0, width, height, Justification::centred, false);
    }
}

//TABLE HEADER
//==============================================================================
void AudiostromLookAndFeel::drawTableHeaderBackground(Graphics &g, TableHeaderComponent &header)
{
    g.fillAll(Colour(186, 186, 186));

    const int w = header.getWidth();
    const int h = header.getHeight();
    g.drawRect(0, 0, w, h, 1);


    for (int i = header.getNumColumns(true); --i >= 0;)
        g.fillRect(header.getColumnPosition(i).getRight() - 1, 0, 1, h);
}

void AudiostromLookAndFeel::drawTableHeaderColumn(Graphics &g, const String &columnName, int /*columnId*/,
                                                  int width, int height,
                                                  bool isMouseOver, bool isMouseDown,
                                                  int columnFlags)
{
    if (isMouseDown)
        g.fillAll(Colour(0x8890ff8d));
    else if (isMouseOver)
        g.fillAll(Colour(0x55d4f0ff));

    int rightOfText = width - 4;

    if ((columnFlags & (TableHeaderComponent::sortedForwards | TableHeaderComponent::sortedBackwards)) != 0)
    {
        const float top = height * ((columnFlags & TableHeaderComponent::sortedForwards) != 0 ? 0.35f : (1.0f - 0.35f));
        const float bottom = height - top;

        const float w = height * 0.5f;
        const float x = rightOfText - (w * 1.25f);
        rightOfText = (int) x;

        Path sortArrow;
        sortArrow.addTriangle(x, bottom, x + w * 0.5f, top, x + w, bottom);

        g.setColour(Colour(0x99000000));
        g.fillPath(sortArrow);
    }

    g.setColour(Colours::black);
    g.setFont(Font(height * 0.5f, Font::bold));
    const int textX = 4;
    g.drawFittedText(columnName, textX, 0, rightOfText - textX, height, Justification::centredLeft, 1);
}

void AudiostromLookAndFeel::paintRowBackground(Graphics &g, int /*rowNumber*/, int width, int height,
                                               bool rowIsSelected)
{

    if (rowIsSelected)
    {
        g.fillAll(AudiostromLookAndFeel::ListSelectColor);
    }
    else
    {
        g.fillAll(ListBackColor);
    }
    g.setColour(Colours::black);
    g.drawLine(0.0f, (float) height, (float) width, (float) height);
    g.setColour(AudiostromLookAndFeel::WhiteTextColor);
}


//POPUP MENU
void AudiostromLookAndFeel::drawPopupMenuBackground(Graphics &g, int width, int height)
{
    g.fillAll(findColour(PopupMenu::backgroundColourId));

    g.setColour(Colours::black.withAlpha(0.6f));
    g.drawRect(0, 0, width, height);
}

Font AudiostromLookAndFeel::getPopupMenuFont()
{
    return Font(15.0f);
}

// COMBO BOX
//==============================================================================
void AudiostromLookAndFeel::drawComboBox(Graphics &g, int width, int height,
                                         const bool isButtonDown,
                                         int buttonX, int buttonY,
                                         int buttonW, int buttonH,
                                         ComboBox &box)
{
    g.fillAll(box.findColour(ComboBox::backgroundColourId));

    g.setColour(box.findColour((isButtonDown) ? ComboBox::buttonColourId
                                              : ComboBox::backgroundColourId));
    g.fillRect(buttonX, buttonY, buttonW, buttonH);

    g.setColour(box.findColour(ComboBox::outlineColourId));
    g.drawRect(0, 0, width, height);

    const float arrowX = 0.2f;
    const float arrowH = 0.3f;

    if (box.isEnabled())
    {
        Path p;
        p.addTriangle(buttonX + buttonW * 0.5f, buttonY + buttonH * (0.45f - arrowH),
                      buttonX + buttonW * (1.0f - arrowX), buttonY + buttonH * 0.45f,
                      buttonX + buttonW * arrowX, buttonY + buttonH * 0.45f);

        p.addTriangle(buttonX + buttonW * 0.5f, buttonY + buttonH * (0.55f + arrowH),
                      buttonX + buttonW * (1.0f - arrowX), buttonY + buttonH * 0.55f,
                      buttonX + buttonW * arrowX, buttonY + buttonH * 0.55f);

        g.setColour(box.findColour((isButtonDown) ? ComboBox::backgroundColourId
                                                  : ComboBox::buttonColourId));
        g.fillPath(p);
    }
}

Font AudiostromLookAndFeel::getComboBoxFont(ComboBox &box)
{
    Font f(jmin(15.0f, box.getHeight() * 0.85f));
    f.setHorizontalScale(0.9f);
    return f;
}

void AudiostromLookAndFeel::paintCell(Graphics &g, int /*rowNumber*/, int /*columnId*/, int /*width*/, int /*height*/,
                                      bool rowIsSelected)
{
    setTableContentFont(g);
    if (rowIsSelected) g.setColour(Colours::black);
    else
        g.setColour(AudiostromLookAndFeel::WhiteTextColor);
}

Font AudiostromLookAndFeel::getTableContentFont()
{
    return Font(13.0f);
}

void AudiostromLookAndFeel::setTableContentFont(Graphics &g)
{
    g.setFont(13.0f);
}


//List box
void AudiostromLookAndFeel::drawListboxItem(String Text, int /*rowNumber*/, Graphics &g, int width, int height,
                                            bool rowIsSelected, Colour overlayColor, Colour fontColour, int hOffset)
{

    if (overlayColor != Colours::transparentBlack)
    {
        g.fillAll(overlayColor);
    }

    if (rowIsSelected)
    {

        if (overlayColor != Colours::transparentBlack)
        {
            g.fillAll(AudiostromLookAndFeel::ListSelectColor.withAlpha(0.5f));
        }
        else
        {
            g.fillAll(AudiostromLookAndFeel::ListSelectColor);
        }

        g.setColour(Colours::black);


    }
    else
    {
        if (fontColour != Colours::transparentBlack)
            g.setColour(fontColour);
        else
        {
            g.setColour(Colours::white);
        }
    }

    g.setFont(15);
    g.drawFittedText(Text, hOffset, 0, width - hOffset - 3, height, Justification::centredLeft, 1, 0.8f);
}

bool AudiostromLookAndFeel::setTextFromInputBox(String DialogTitle, String TextTitle, String &text)
{
    AlertWindow namewindow(DialogTitle, String(), AlertWindow::NoIcon);
    namewindow.addTextEditor("NameBox", text, TextTitle, false);
    namewindow.addButton(TRANS("OK"), 1, KeyPress(KeyPress::returnKey, 0, 0));
    namewindow.addButton(TRANS("Cancel"), 0, KeyPress(KeyPress::escapeKey, 0, 0));
    if (namewindow.runModalLoop() != 0)
    {
        text = namewindow.getTextEditorContents("NameBox");
        return true;
    }
    else return false;
}

//ALERT Window
void AudiostromLookAndFeel::drawAlertBox(Graphics &g, AlertWindow &alert, const Rectangle<int> &textArea,
                                         TextLayout &textLayout)
{
    g.fillAll(alert.findColour(AlertWindow::backgroundColourId));

    int iconSpaceUsed = 0;

    //const int iconWidth = 80;
    //int iconSize = jmin (iconWidth + 50, alert.getHeight() + 20);

    //if (alert.containsAnyExtraComponents() || alert.getNumButtons() > 2)
    //    iconSize = jmin (iconSize, textArea.getHeight() + 50);

    //const Rectangle<int> iconRect (iconSize / -10, iconSize / -10,
    //                               iconSize, iconSize);

    if (alert.getAlertType() != AlertWindow::NoIcon)
    {


        std::unique_ptr<Drawable> iconImage = Drawable::createFromImageData(BinaryData::QuestionSymbol_svg,
                                                                            BinaryData::QuestionSymbol_svgSize);

        if (alert.getAlertType() == AlertWindow::WarningIcon)
        {

            iconImage = Drawable::createFromImageData(BinaryData::WarningSymbol_svg, BinaryData::WarningSymbol_svgSize);
        }
        else
        {
            if (alert.getAlertType() == AlertWindow::InfoIcon)
            {
                iconImage = Drawable::createFromImageData(BinaryData::InformationSymbol_svg,
                                                          BinaryData::InformationSymbol_svgSize);
            }
            else
            {
                iconImage = Drawable::createFromImageData(BinaryData::QuestionSymbol_svg,
                                                          BinaryData::QuestionSymbol_svgSize);
            }
        }

        Rectangle<float> iconarea;
        iconarea.setWidth(70);
        iconarea.setHeight(70);
        iconarea.setX(15.0f);
        iconarea.setY((alert.getHeight() / 2) - (iconarea.getHeight() / 2) - 5);
        iconSpaceUsed = roundToInt(iconarea.getWidth() + iconarea.getX());
        iconImage->drawWithin(g, iconarea, RectanglePlacement::stretchToFit, 1.0f);

    }

    g.setColour(alert.findColour(AlertWindow::textColourId));

    textLayout.draw(g, Rectangle<int>(textArea.getX() + iconSpaceUsed,
                                      textArea.getY(),
                                      textArea.getWidth() - iconSpaceUsed,
                                      textArea.getHeight()).toFloat());

    g.setColour(alert.findColour(AlertWindow::outlineColourId));
    g.drawRect(0, 0, alert.getWidth(), alert.getHeight());
}


Font AudiostromLookAndFeel::getAlertWindowMessageFont()
{
    return Font(15);
}

Font AudiostromLookAndFeel::getAlertWindowFont()
{
    return Font(18);
}

Colour AudiostromLookAndFeel::getLightColorFromHash(int hash)
{
    int index = hash % lightColorPalette.size();
    return lightColorPalette[index];
}

void AudiostromLookAndFeel::drawLevelMeter(Graphics &g, int width, int height, float level)
{
    g.setColour(Colours::black.withAlpha(0.8f));
    g.fillRoundedRectangle(0.0f, 0.0f, (float) width, (float) height, 3.0f);
    g.setColour(Colours::white.withAlpha(0.15f));
    g.drawRoundedRectangle(1.0f, 1.0f, width - 2.0f, height - 2.0f, 3.0f, 1.0f);

    const int totalBlocks = 7;
    const int numBlocks = roundToInt(totalBlocks * level);
    const float w = (width - 6.0f) / (float) totalBlocks;

    for (int i = 0; i < totalBlocks; ++i)
    {
        if (i >= numBlocks)
            g.setColour(PanelBackground.withAlpha(0.6f));
        else
            g.setColour(i < totalBlocks - 1 ? OnGreen
                                            : OnRed);

        g.fillRoundedRectangle(3.0f + i * w + w * 0.1f, 3.0f, w * 0.8f, height - 6.0f, w * 0.1f);
    }
}

void AudiostromLookAndFeel::paintToolbarButtonBackground(Graphics &g, int width, int height, bool isMouseOver,
                                                         bool isMouseDown, ToolbarItemComponent &component)
{
    if (isMouseDown)
        g.fillAll(component.findColour(Toolbar::buttonMouseDownBackgroundColourId, true));
    else if (isMouseOver)
        g.fillAll(component.findColour(Toolbar::buttonMouseOverBackgroundColourId, true));

}

void AudiostromLookAndFeel::paintToolbarBackground(Graphics &g, int w, int h, Toolbar &toolbar)
{
    const Colour background(toolbar.findColour(Toolbar::backgroundColourId));

    g.setGradientFill(ColourGradient(background, 0.0f, 0.0f,
                                     background.darker(0.1f),
                                     toolbar.isVertical() ? w - 1.0f : 0.0f,
                                     toolbar.isVertical() ? 0.0f : h - 1.0f,
                                     false));
    g.fillAll();
}

//SLIDER
//SLIDER (Fader)
void AudiostromLookAndFeel::drawLinearSlider(Graphics &g, int x, int y, int width, int height,
                                             float sliderPos, float minSliderPos, float maxSliderPos,
                                             const Slider::SliderStyle style, Slider &slider)
{

    if (style == Slider::SliderStyle::LinearVertical)
    {

        //Draw fader knob
        //Get the Colour
        Colour knobCl = slider.findColour(Slider::ColourIds::thumbColourId);
        //Create the gadient
        ColourGradient grad(knobCl.darker(0.4f), 0.0f, 0.0f, knobCl.brighter(0.1f), 0.0f, 1.0f, false);

        //Calc position and size
        float knobWidth = width * 0.7f;
        float knobHeight = knobWidth * 1.7f;
        float knobX = (width - knobWidth) * 0.5f;
        float knobY = (sliderPos / (y + height)) * (y + height - knobHeight);

        g.setColour(Colours::dimgrey);

        float dbPos =
                ((slider.getPositionOfValue(1.0f) / (y + height)) * (y + height - knobHeight)) + (knobHeight * 0.5f);
        g.drawHorizontalLine(roundToInt(dbPos), float(x), float(x + width));

        dbPos = ((slider.getPositionOfValue(0.5f) / (y + height)) * (y + height - knobHeight)) + (knobHeight * 0.5f);
        g.drawHorizontalLine(roundToInt(dbPos), float(x), float(x + width));

        dbPos = ((slider.getPositionOfValue(0.316f) / (y + height)) * (y + height - knobHeight)) + (knobHeight * 0.5f);
        g.drawHorizontalLine(roundToInt(dbPos), float(x), float(x + width));

        dbPos = ((slider.getPositionOfValue(0.1f) / (y + height)) * (y + height - knobHeight)) + (knobHeight * 0.5f);
        g.drawHorizontalLine(roundToInt(dbPos), float(x), float(x + width));


        //Draw track/background (2px box)
        g.setColour(Colours::black);
        float bx = x + (width / 2.0f) - 1.0f;
        g.fillRect(bx, (float) y, 2.0f, (float) height);

        //Draw a "shadow"
        //g.setColour(Colours::black.withAlpha(0.2f));
        //g.fillRoundedRectangle(x, y, width, height,width*0.4f);


        //The knob is made up of three gradient boxes:
        Rectangle<float> drawRect(knobX, knobY, knobWidth, knobHeight * 0.25f);
        grad.point1.setY(drawRect.getY());
        grad.point2.setY(drawRect.getBottom());
        g.setGradientFill(grad);
        g.fillRect(drawRect);

        //Draw the centre
        drawRect.setY(drawRect.getY() + drawRect.getHeight());
        drawRect.setHeight(knobHeight * 0.5f);
        grad.point1.setY(drawRect.getY());
        grad.point2.setY(drawRect.getBottom());
        g.setGradientFill(grad);
        g.fillRect(drawRect);

        //Draw the bottom
        drawRect.setY(drawRect.getY() + drawRect.getHeight());
        drawRect.setHeight(knobHeight * 0.25f);
        grad.point1.setY(drawRect.getY());
        grad.point2.setY(drawRect.getBottom());
        g.setGradientFill(grad);
        g.fillRect(drawRect);
        //Draw line in centre and rectangle around it
        g.setColour(Colours::black);
        g.drawHorizontalLine(roundToInt(knobY + (knobHeight * 0.5f)), knobX, knobX + knobWidth);
        g.drawRect(knobX - 0.1f, knobY, knobWidth + 0.2f, knobHeight, 1.5f);


    }
    else
    {
        LookAndFeel_V3::drawLinearSlider(g, x, y, width, height, sliderPos, minSliderPos, maxSliderPos, style, slider);
    }

}


/*   SPECIAL LOOKS */
void SpecialLooks::createTabButtonShape(TabBarButton &button, Path &p, bool /*isMouseOver*/, bool /*isMouseDown*/)
{
    const Rectangle<int> activeArea(button.getActiveArea());
    const float w = (float) activeArea.getWidth();
    const float h = (float) activeArea.getHeight();

    float length = w;
    float depth = h;

    if (button.getTabbedButtonBar().isVertical())
        std::swap(length, depth);

    const float indent = (float) getTabButtonOverlap((int) depth);
    const float overhang = 4.0f;

    switch (button.getTabbedButtonBar().getOrientation())
    {
        case TabbedButtonBar::TabsAtLeft:
            /*p.startNewSubPath (w, 0.0f);
            p.lineTo (0.0f, indent);
            p.lineTo (0.0f, h - indent);
            p.lineTo (w, h);
            p.lineTo (w + overhang, h + overhang);
            p.lineTo (w + overhang, -overhang);*/
            p.addRectangle(0, 0, w - 2, h);
            break;

        case TabbedButtonBar::TabsAtRight:
            p.startNewSubPath(0.0f, 0.0f);
            p.lineTo(w, indent);
            p.lineTo(w, h - indent);
            p.lineTo(0.0f, h);
            p.lineTo(-overhang, h + overhang);
            p.lineTo(-overhang, -overhang);
            break;

        case TabbedButtonBar::TabsAtBottom:
            p.startNewSubPath(0.0f, 0.0f);
            p.lineTo(indent, h);
            p.lineTo(w - indent, h);
            p.lineTo(w, 0.0f);
            p.lineTo(w + overhang, -overhang);
            p.lineTo(-overhang, -overhang);
            break;

        default:
            p.startNewSubPath(0.0f, h);
            p.lineTo(indent, 0.0f);
            p.lineTo(w - indent, 0.0f);
            p.lineTo(w, h);
            p.lineTo(w + overhang, h + overhang);
            p.lineTo(-overhang, h + overhang);
            break;
    }

    p.closeSubPath();

    p = p.createPathWithRoundedCorners(3.0f);
}

void SpecialLooks::fillTabButtonShape(TabBarButton &button, Graphics &g, const Path &path, bool /*isMouseOver*/,
                                      bool /*isMouseDown*/)
{
    const Colour tabBackground = PanelBackground;
    const bool isFrontTab = button.isFrontTab();

    g.setColour(isFrontTab ? tabBackground
                           : tabBackground.withMultipliedAlpha(0.3f));

    g.fillPath(path);

    /*g.setColour (button.findColour (isFrontTab ? TabbedButtonBar::frontOutlineColourId
        : TabbedButtonBar::tabOutlineColourId, false)
        .withMultipliedAlpha (button.isEnabled() ? 1.0f : 0.5f));*/
    g.setColour(Colours::black);

    g.strokePath(path, PathStrokeType(1.0f));
}

void SpecialLooks::drawTabButtonText(TabBarButton &button, Graphics &g, bool /*isMouseOver*/, bool /*isMouseDown*/)
{
    const Rectangle<float> area(button.getTextArea().toFloat());

    float length = area.getWidth();
    float depth = area.getHeight();

    Font font(15);
    Colour col;

    col = juce::Colours::white;
    if (button.isFrontTab())
    {
        g.setColour(col);
    }
    else
    {
        g.setColour(col.darker(0.1f));
    }

    g.setFont(font);


    //g.drawText(button.getButtonText(),0,0,length,depth,Justification::centred,true);
    g.drawFittedText(button.getButtonText(), 0, 2, roundToInt(length), roundToInt(depth), Justification::centred, 2,
                     0.9f);
/*	if (button.getTabbedButtonBar().isVertical())
		std::swap (length, depth);

	Font font (depth * 0.6f);
	font.setUnderline (button.hasKeyboardFocus (false));

	AffineTransform t;

	switch (button.getTabbedButtonBar().getOrientation())
	{
	case TabbedButtonBar::TabsAtLeft:  // t = t.rotated (float_Pi * -0.5f).translated (area.getX(), area.getBottom()); break;
	case TabbedButtonBar::TabsAtRight:  t = t.rotated (float_Pi *  0.5f).translated (area.getRight(), area.getY()); break;
	case TabbedButtonBar::TabsAtTop:
	case TabbedButtonBar::TabsAtBottom: t = t.translated (area.getX(), area.getY()); break;
	default:                            jassertfalse; break;
	}

	Colour col;

	if (button.isFrontTab() && (button.isColourSpecified (TabbedButtonBar::frontTextColourId)
		|| isColourSpecified (TabbedButtonBar::frontTextColourId)))
		col = findColour (TabbedButtonBar::frontTextColourId);
	else if (button.isColourSpecified (TabbedButtonBar::tabTextColourId)
		|| isColourSpecified (TabbedButtonBar::tabTextColourId))
		col = findColour (TabbedButtonBar::tabTextColourId);
	else
		col = button.getTabBackgroundColour().contrasting();

	const float alpha = button.isEnabled() ? ((isMouseOver || isMouseDown) ? 1.0f : 0.8f) : 0.3f;

	g.setColour (col.withMultipliedAlpha (alpha));
	g.setFont (font);
	g.addTransform (t);

	g.drawFittedText (button.getButtonText().trim(),
		0, 0, (int) length, (int) depth,
		Justification::centred,
		jmax (1, ((int) depth) / 12));*/
}

void SpecialLooks::drawTabButton(TabBarButton &button, Graphics &g, bool isMouseOver, bool isMouseDown)
{
    Path tabShape;
    createTabButtonShape(button, tabShape, isMouseOver, isMouseDown);

    const Rectangle<int> activeArea(button.getActiveArea());
    tabShape.applyTransform(AffineTransform::translation((float) activeArea.getX(),
                                                         (float) activeArea.getY()));

    DropShadow(Colours::black.withAlpha(0.5f), 2, Point < int > (0, 1)).drawForPath(g, tabShape);

    fillTabButtonShape(button, g, tabShape, isMouseOver, isMouseDown);
    drawTabButtonText(button, g, isMouseOver, isMouseDown);
}


int SpecialLooks::getTabButtonBestWidth(TabBarButton & /*button*/, int /*tabDepth*/)
{
    return 40;

}

int SpecialLooks::getTabButtonOverlap(int /*tabDepth*/)
{
    return 0;

}

int SpecialLooks::getTabButtonSpaceAroundImage()
{
    return 4;
}
