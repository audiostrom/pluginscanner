/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.6

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "MainComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
MainComponent::MainComponent (int pluginFormat)
{
    //[Constructor_pre] You can add your own custom stuff here..
	theLook.reset(new AudiostromLookAndFeel());
	LookAndFeel::setDefaultLookAndFeel(theLook.get());

	progValue = 0.0f;
	scanMaster.reset(new ScanMaster(pluginFormat));
	scanMaster->addChangeListener(this);
    //[/Constructor_pre]

    progBar.reset (new ProgressBar (progValue));
    addAndMakeVisible (progBar.get());
    progBar->setName ("progBar");

    label.reset (new Label ("new label",
                            TRANS("Testing Plugins:")));
    addAndMakeVisible (label.get());
    label->setFont (Font (20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (Label::textColourId, Colours::white);
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    lastPlugLabel.reset (new Label ("lastPlugLabel",
                                    TRANS("-")));
    addAndMakeVisible (lastPlugLabel.get());
    lastPlugLabel->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    lastPlugLabel->setJustificationType (Justification::centred);
    lastPlugLabel->setEditable (false, false, false);
    lastPlugLabel->setColour (Label::textColourId, Colours::white);
    lastPlugLabel->setColour (TextEditor::textColourId, Colours::black);
    lastPlugLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    test.reset (new TextButton ("test"));
    addAndMakeVisible (test.get());
    test->setButtonText (TRANS("Cancel"));
    test->addListener (this);

    test->setBounds (209, 144, 96, 24);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (520, 200);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

MainComponent::~MainComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
	scanMaster->removeChangeListener(this);
    //[/Destructor_pre]

    progBar = nullptr;
    label = nullptr;
    lastPlugLabel = nullptr;
    test = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff424245));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void MainComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    progBar->setBounds (proportionOfWidth (0.0500f), 80, proportionOfWidth (0.9000f), 24);
    label->setBounds (0, 32, proportionOfWidth (1.0000f), 24);
    lastPlugLabel->setBounds (8, 108, getWidth() - 16, 24);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void MainComponent::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == test.get())
    {
        //[UserButtonCode_test] -- add your button handler code here..
		JUCEApplication::quit();
        //[/UserButtonCode_test]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void MainComponent::changeListenerCallback(ChangeBroadcaster* source)
{
	progValue = scanMaster->getProgress();
	lastPlugLabel->setText(scanMaster->getPlugName(), dontSendNotification);

}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="MainComponent" componentName=""
                 parentClasses="public Component, public ChangeListener" constructorParams="int pluginFormat"
                 variableInitialisers="" snapPixels="8" snapActive="1" snapShown="1"
                 overlayOpacity="0.330" fixedSize="1" initialWidth="520" initialHeight="200">
  <BACKGROUND backgroundColour="ff424245"/>
  <GENERICCOMPONENT name="progBar" id="17c3473b06fbee4a" memberName="progBar" virtualName="ProgressBar"
                    explicitFocusOrder="0" pos="5% 80 90% 24" class="ProgressBar"
                    params="progValue"/>
  <LABEL name="new label" id="d774deb1b579b357" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="0 32 100% 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="Testing Plugins:"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default font" fontsize="20.0" kerning="0.0" bold="0"
         italic="0" justification="36"/>
  <LABEL name="lastPlugLabel" id="75cd1a93eee1b98b" memberName="lastPlugLabel"
         virtualName="" explicitFocusOrder="0" pos="8 108 16M 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="-" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <TEXTBUTTON name="test" id="866d7e451fdf4800" memberName="test" virtualName=""
              explicitFocusOrder="0" pos="209 144 96 24" buttonText="Cancel"
              connectedEdges="0" needsCallback="1" radioGroupId="0"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

