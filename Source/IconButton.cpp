/*
  ==============================================================================

    IconButton.cpp
    Created: 1 Apr 2014 12:35:59pm
    Author:  Nikolai

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "IconButton.h"

//==============================================================================
IconButton::IconButton(const String buttonName, const String toolTip) :
Button(buttonName),
currentImage(nullptr),
minCorenerSize(3),
padding(0),
round(false),
roundedCorners(false)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
	setButtonText(String());
	setTooltip(toolTip);
}

IconButton::~IconButton()
{
	currentImage = nullptr;
	normalImage = nullptr;
	downImage = nullptr;
	onImage = nullptr;
}



void IconButton::resized()
{
	Button::resized();

	if (currentImage != nullptr)
	{
		Rectangle<int> imageSpace;
		//int devider = JUCE_LIVE_CONSTANT(10);
		//int min = JUCE_LIVE_CONSTANT(3);
		imageSpace = getLocalBounds().reduced(jmax((getWidth() + (padding * 2)) / 6.0f, 3.0f), jmax((getHeight() + (padding * 2)) / 6.0f, 3.0f));
		currentImage->setTransformToFit(imageSpace.toFloat(), RectanglePlacement::centred);
	}

	if (((getHeight() <= 30) || (getWidth() <= 30)) && (!round))
	{
		setOpaque(true);
		roundedCorners = false;
	}
	else
	{
		setOpaque(false);
		roundedCorners = true;
	}
}

void IconButton::paintButton(Graphics& g, bool isMouseOverButton, bool isButtonDown)
{

	Colour backC = getToggleState() ? findColour(backgroundOnColourId, true) : findColour(backgroundColourId, true);
	Colour txtColor = Colours::white;
	if (backC.getBrightness() > 0.6f) txtColor = Colours::black;

	if (isOver()) backC = backC.brighter(0.15f);
	if (!isEnabled()) backC = backC.darker(0.35f);
	if (isDown()) backC = backC.darker();

	if (!round)
	{
	
		if (roundedCorners) //Rounded coreners
		{
	
		float cornerH = jmax((getHeight()*0.15f), minCorenerSize);
		float cornerW = jmax((getWidth()*0.15f), minCorenerSize);
		float corner = jmin(cornerH, cornerW);
		g.setColour(backC.darker(1.5f));
		g.fillRoundedRectangle(float(0.0f), 0, (float)getWidth(), (float)getHeight(), corner);
		g.setColour(backC);
		g.fillRoundedRectangle(float(1.0f), 1, (float)getWidth() - 2, (float)getHeight() - 2, corner);
		g.setColour(txtColor);
		g.drawFittedText(getButtonText(), 2, 1, getWidth() - 4,getHeight() - 2, Justification::centred,2,1.0f);
		}
		else //Square
		{
	
			g.setColour(backC);
			g.fillRect(0,0, getWidth(), getHeight());

            g.setColour(txtColor);
            g.drawFittedText(getButtonText(), 2, 1, getWidth() - 4, getHeight() - 2, Justification::centred, 2, 1.0f);

			g.setColour(backC.darker(1.5f));
			g.drawRect(0, 0, getWidth(), getHeight(),1);

		}
	}
	else
	{ //Draw a circular button
		g.setColour(backC);
		float size = jmin((float)getWidth(), (float)getHeight());
		g.fillEllipse((padding / 2.0f)+(((float)getWidth()-size)/2.0f), 0.0f + (padding / 2.0f), size - padding, size - padding);
		g.setColour(backC.darker(2.5f));
		g.drawEllipse((padding / 2.0f) + (((float)getWidth() - size) / 2.0f), 0.0f + (padding / 2.0f), size - padding, size - padding, 1);

	}


}


void IconButton::setImages(const Drawable* normal, const Drawable* down /*= nullptr*/, const Drawable* onimg /*= nullptr*/)
{
	if (normal != nullptr) normalImage = normal->createCopy();
    if (down != nullptr) downImage = down->createCopy();
    if (onimg != nullptr) onImage = onimg->createCopy();
	buttonStateChanged();
}


void IconButton::setOnColor(Colour cl)
{
    setColour(backgroundOnColourId, cl);
}

void IconButton::buttonStateChanged()
{
	//Button::buttonStateChanged();
	repaint();
	Drawable* imageToDraw = nullptr;
	float opacity = isEnabled() ? 1.0f : 0.4f;

	if (isDown()) imageToDraw = downImage.get();
	if (getToggleState())
	{
		imageToDraw = (onImage != nullptr)? onImage.get() : downImage.get();
	}

	if (imageToDraw == nullptr) imageToDraw = normalImage.get();

	if (imageToDraw != currentImage)
	{
		removeChildComponent(currentImage);
		currentImage = imageToDraw;

		if (currentImage != nullptr)
		{
			currentImage->setInterceptsMouseClicks(false, false);
			addAndMakeVisible(currentImage);
			IconButton::resized();
		}
	}

	if (currentImage != nullptr)
		currentImage->setAlpha(opacity);
}

void IconButton::enablementChanged()
{
	Button::enablementChanged();
	buttonStateChanged();
}

void IconButton::setRound(bool value)
{
	round = value;
	setOpaque(false);
}

void IconButton::setPadding(float pad)
{
	padding = pad;
}
